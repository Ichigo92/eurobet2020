<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class RankingController extends Controller
{
    public function index()
    {
        return view('rankings', [
            'title' => 'User Rankings',
            'users' => User::orderBy('points', 'desc')->paginate()
        ]);
    }
}
