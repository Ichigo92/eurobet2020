<?php

namespace App\Http\Controllers;

use App\Models\Bet;
use App\Models\TheMatch;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {

        if (Auth::user()) {
            $user_bets = Bet::select('match_id')->where(['user_id' => Auth::user()->id])->get()->keyBy('match_id')->toArray();
        }

        return view('homepage', [
            'title' => 'Homepage',
            'matches' => TheMatch::with('team1', 'team2', 'winnerTeam')
                ->orderBy('date_start', 'desc')
                ->paginate(20),
            'user_bets' => $user_bets ?? null
        ]);
    }
}
