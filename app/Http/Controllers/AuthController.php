<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function loginView()
    {

        return view('auth.login', [
            'title' => 'Login'
        ]);
    }

    public function login(Request $request)
    {

        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        //$user = User::where('email', $request->input('email'))->first();

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            return redirect()->route('homepage');
        }

        return redirect()->back()->withErrors([
            'login-error'  => 'Wrong credentials'
        ]);

    }

    public function signinView()
    {
        return view('auth.registration', [
            'title' => 'Sign In'
        ]);
    }

    public function signin(Request $request) :RedirectResponse
    {
        $request->validate([
            'name' => 'required|min:3|max:200',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6'
        ]);

        User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password'))
        ]);

        return redirect()->route('homepage');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('homepage');
    }

}
