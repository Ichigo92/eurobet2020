<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamsController extends Controller
{
    public function index()
    {
        return view('admin.teams', [
            'title' => 'Teams',
            'teams' => DB::table('teams')->paginate()
        ]);
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3|max:100|unique:teams'
        ]);

        Team::create([
            'name' => $request->input('name')
        ]);

        return redirect()->route('teams');

    }
}
