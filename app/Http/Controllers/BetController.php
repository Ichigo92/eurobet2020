<?php

namespace App\Http\Controllers;

use App\Models\Bet;
use App\Models\TheMatch;
use Illuminate\Support\Facades\Auth;

class BetController extends Controller
{
    public function index(TheMatch $theMatch)
    {
        return view('bet', [
            'title' => "Bet",
            'match' => $theMatch->load('userHasBet'),

        ]);
    }

    public function create(TheMatch $theMatch, int $winnerChoice)
    {

        $theMatch->load('userHasBet');

        if ($theMatch->userHasBet) {
            return redirect()->back()->withErrors([
                'error' => 'You have already bet on this match'
            ]);
        }

        Bet::create([
            'user_id' => Auth::user()->id,
            'match_id' => $theMatch->id,
            'bet' => $winnerChoice
        ]);


        return redirect()->route('homepage');
    }

    public function list(TheMatch $theMatch)
    {

        $theMatch->load(['team1', 'team2']);
        $theMatch->setRelation('bets', $theMatch->bets()->with(['user', 'betTeam'])->paginate());

        return view('bet-list', [
            'title' => "Bet List for math #{$theMatch}",
            'match' => $theMatch,
        ]);
    }
}
