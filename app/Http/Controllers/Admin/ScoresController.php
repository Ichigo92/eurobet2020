<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Models\TheMatch;
use Illuminate\Http\Request;

class ScoresController extends Controller
{
    public function index(TheMatch $theMatch = null)
    {
        return view('admin.scores', [
            'title' => 'Scores',
            'matches' => $theMatch ? [$theMatch] : TheMatch::endedMatches()->withoutWinner()->orderBy('date_start', 'desc')->get()
        ]);
    }

    public function create(Request $request, TheMatch $theMatch)
    {

        if ($request->input('winner') !== 'pair') {
            $teamWinner = Team::findOrFail($request->input('winner'));
            $theMatch->winner_id = $teamWinner->id;
        } else {
            $theMatch->winner_id = TheMatch::PAIR;
        }

        $theMatch->save();

        return redirect()->route('scores');

    }
}
