<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TheMatch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MatchesController extends Controller
{
    public function index()
    {

        return view('admin.matches', [
            'title' => 'Matches',
            'teams' =>  DB::table('teams')->get(['id', 'name']),
            'matches' => TheMatch::with(['team1', 'team2'])
                ->orderBy('date_start', 'desc')
                ->paginate()
        ]);
    }

    public function create(Request $request)
    {
        $request->validate([
            'team_1' => 'required',
            'team_2' => 'required',
            'date_start' => 'required'
        ]);

        // TODO: check creazione match la data di start dev'essere almeno 3 ore in avanti ?
        // TODO: bisogna inserire un check per far in modo che non si scontri lo stesso team o basta solo lato frontend?

        TheMatch::create([
            'team_1' => $request->input('team_1'),
            'team_2' => $request->input('team_2'),
            'date_start' => new \DateTime($request->input('date_start'))
        ]);

        return redirect()->route('matches')->with([
            'success' => 'Match created'
        ]);
    }
}
