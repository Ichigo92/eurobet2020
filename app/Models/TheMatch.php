<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TheMatch extends Model
{

    const PAIR = 0;

    use HasFactory;

    protected $table = 'matches';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_1',
        'team_2',
        'date_start'
    ];

    public function team1()
    {
        return $this->hasOne(Team::class, 'id', 'team_1');
    }

    public function team2()
    {
        return $this->hasOne(Team::class, 'id', 'team_2');
    }

    public function scopeEndedMatches($query)
    {
        $now = Carbon::now();
        return $query->whereRaw("DATE_ADD(date_start, INTERVAL 2 HOUR) < '{$now}'");
    }

    public function scopeWithoutWinner($query)
    {
        return $query->whereNull('winner_id');
    }

    public function userHasBet()
    {
        return $this->hasOne(Bet::class, 'match_id', 'id')->where('user_id', Auth::user()->id);
    }

    public function bets()
    {
        return $this->hasMany(Bet::class, 'match_id', 'id');
    }

    public function winnerTeam()
    {
        return $this->hasOne(Team::class, 'id', 'winner_id');
    }

    public function getIsFinishedAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->date_start)->add('2 hour')->format('Y-m-d H:i:s') < Carbon::now();
    }

    public function getIsPlayingAttribute()
    {
        return Carbon::now() > Carbon::createFromFormat('Y-m-d H:i:s', $this->date_start) &&
            Carbon::now() < Carbon::createFromFormat('Y-m-d H:i:s', $this->date_start)->add('2 hour')->format('Y-m-d H:i:s');
    }

    public function getIsBettableAttribute()
    {
        return Carbon::now()->lessThan(Carbon::parse($this->date_start)->sub('30 minutes'));
    }

    static public function boot()
    {
        parent::boot();

        self::updated(function (TheMatch $theMatch) {
            $bets = $theMatch->bets()->get();
            $bets->each(function ($bet) use ($theMatch) {
                if ($bet->bet === $theMatch->winner_id) {
                    $user = $bet->user()->first();
                    $user->points++;
                    $user->save();
                }
            });
        });
    }
}
