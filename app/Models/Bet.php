<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bet extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'match_id',
        'bet'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function match()
    {
        return $this->hasMany(TheMatch::class, 'id', 'match_id');
    }

    public function betTeam()
    {
        return $this->hasOne(Team::class, 'id', 'bet');
    }
}
