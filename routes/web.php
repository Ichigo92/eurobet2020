<?php

use App\Http\Controllers\Admin\MatchesController;
use App\Http\Controllers\Admin\ScoresController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BetController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RankingController;
use App\Http\Controllers\TeamsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [HomeController::class, 'index'])->name('homepage');

Route::middleware(['guest'])->group(function () {
    Route::get('/login', [AuthController::class, 'loginView'])->name('login-view')->middleware('guest');
    Route::post('/login', [AuthController::class, 'login'])->name('login')->middleware('guest');
    Route::get('/signin', [AuthController::class, 'signinView'])->name('signin-view');
    Route::post('/signin', [AuthController::class, 'signin'])->name('signin');
});


Route::middleware(['auth'])->group(function () {
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

    Route::get('rankings', [RankingController::class, 'index'])->name('rankings');

    Route::get('bet/{theMatch}/list', [BetController::class, 'list'])->name('bets-list');
    Route::get('bet/{theMatch}', [BetController::class, 'index'])->name('match-bet');
    Route::get('bet/{theMatch}/{winnerChoice}', [BetController::class, 'create'])->name('create-bet');

    Route::middleware(['isAdmin'])->group(function () {
        Route::get('admin/matches', [MatchesController::class, 'index'])->name('matches');
        Route::get('admin/teams', [TeamsController::class, 'index'])->name('teams');
        Route::post('admin/teams', [TeamsController::class, 'create'])->name('create-team');
        Route::post('admin/matches', [MatchesController::class, 'create'])->name('create-match');

        Route::get('admin/scores/{theMatch?}', [ScoresController::class, 'index'])->name('scores');
        Route::post('admin/scores/{theMatch}/create', [ScoresController::class, 'create'])->name('create-score');
    });
});
