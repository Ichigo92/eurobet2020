@extends('layouts/master')

@section('title', $title)

@section('content')

    @if($errors->has('error'))
        <div class="row">
            <div class="col-md-12">
                <span class="alert-danger text-center">{{ $errors->first('error') }}</span>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="justify-content-center">
                <div class="card">
                    <h3 class="card-title text-center">
                        {{ $match->team1->name }} <i class="fa fa-close fa-fw"></i> {{ $match->team2->name }} - {{ $match->date_start }}
                    </h3>
                    <div class="card-body">

                        @if($match->userHasBet)
                            <span class="alert-danger text-center">You have already bet on this match!</span>
                        @else
                            <div class="row">
                                <div class="col-md-4 mb-3">
                                    <a class="btn btn-block btn-outline-success" href="{{ route('create-bet', [$match->id, $match->team1->id]) }}">{{ $match->team1->name }}</a>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <a class="btn btn-block btn-outline-success" href="{{ route('create-bet', [$match->id, 0]) }}">Pair</a>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <a class="btn btn-block btn-outline-success" href="{{ route('create-bet', [$match->id, $match->team2->id]) }}">{{ $match->team2->name }}</a>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

@endsection
