@extends('layouts/master')

@section('title', $title)

@section('content')

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <h3 class="card-header text-center">Team List</h3>

                <div class="card-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($teams->count() == 0)
                            <tr>
                                <td colspan="5">No teams to display.</td>
                            </tr>
                        @endif

                        @foreach ($teams as $index => $team)
                            <tr>
                                <td> {{ ($teams ->currentpage()-1) * $teams ->perpage() + $loop->index + 1 }} </td>
                                <td> {{ $team->name }} </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    {{ $teams->links('vendor.pagination.bootstrap-4') }}

                    <p>
                        Displaying {{$teams->count()}} of {{ $teams->total() }} team(s).
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <h3 class="card-header text-center">Add Team</h3>
                <div class="card-body">
                    <form action="{{ route('create-team') }}" method="POST">
                        @csrf
                        <div class="form-group mb-3">
                            <label for="name">Team Name: </label>
                            <input type="text" placeholder="Team Name" id="name" class="form-control" name="name"
                                   required
                                   autofocus>
                            @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>

                        <div class="d-grid mx-auto">
                            <button type="submit" class="btn btn-dark btn-block">Add Team</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

@endsection
