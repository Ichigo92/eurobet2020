@extends('layouts/master')

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h3 class="card-header text-center">Match scores</h3>
                <div class="card-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>

                            <th>Team 1</th>
                            <th>Team 2</th>
                            <th>Ended at</th>
                            <th>Winner</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($matches as $match)
                            <tr>

                                <td class="align-middle">{{ $match->team1->name }}</td>
                                <td class="align-middle">{{ $match->team2->name }}</td>
                                <td class="align-middle">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $match->date_start)->add('2 hour')->format('Y-m-d H:i:s') }}</td>
                                <td class="text-center">
                                    <form action="{{ route('create-score', ['theMatch' => $match->id]) }}" method="POST">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>Team 1</th>
                                                <th>Pair</th>
                                                <th>Team 2</th>
                                                <th>Set</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="text-center align-middle">
                                                    <input name="winner" type="radio" value="{{ $match->team1->id }}">
                                                </td>
                                                <td class="text-center align-middle">
                                                    <input name="winner" type="radio" value="pair">
                                                </td>
                                                <td class="text-center align-middle">
                                                    <input name="winner" type="radio" value="{{ $match->team2->id }}">
                                                </td>
                                                <td class="text-center align-middle">
                                                    <button class="btn btn-outline-primary" type="submit">Set</button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        @csrf

                                    </form>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

@endsection
