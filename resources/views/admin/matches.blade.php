@extends('layouts/master')

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <h3 class="card-header text-center">Create Match</h3>
                <div class="card-body">

                    <form action="{{ route('create-match') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <label for="team-1">Team 1:</label>
                                <select class="form-control" id="team-1" name="team_1">
                                    <option value="">Select Team</option>
                                    @foreach($teams as $team)
                                        <option value="{{ $team->id }}"> {{$team->name}} </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('team_1'))
                                    <span class="text-danger">{{ $errors->first('team_1') }}</span>
                                @endif
                            </div>


                            <div class="col-md-12">
                                <label for="team-2">Team 2:</label>
                                <select class="form-control" id="team-2" name="team_2">
                                    <option value="">Select Team</option>
                                    @foreach($teams as $team)
                                        <option value="{{ $team->id }}"> {{$team->name}} </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('team_2'))
                                    <span class="text-danger">{{ $errors->first('team_2') }}</span>
                                @endif
                            </div>

                            <div class="col-md-12">
                                <label for="match-date">Date:</label>
                                <input id="match-date" class="form-control" type="datetime-local" name="date_start">
                                @if ($errors->has('date_start'))
                                    <span class="text-danger">{{ $errors->first('date_start') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mt-3">
                                <button type="submit" class="btn btn-dark btn-block">Add Match</button>
                            </div>
                        </div>
                        @if(session()->has('success'))
                            <div class="row">
                                <div class="col-md-12 mt-3">
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>


        </div>
        <div class="col-md-6">
            <div class="card">
                <h3 class="card-header text-center">Match List</h3>
                <div class="card-body">
                    <table class="table table-bordered table-hover table-responsive">
                        <thead>
                        <tr>
                            <th> #</th>
                            <th> Team 1</th>
                            <th> Team 2</th>
                            <th> Start Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($matches->count() == 0)
                            <tr>
                                <td colspan="4">No Match to display.</td>
                            </tr>
                        @endif

                        @foreach ($matches as $index => $match)

                            <tr>
                                <td> {{ ($matches->currentpage()-1) * $matches->perpage() + $loop->index + 1 }} </td>
                                <td> {{ $match->team1->name }} </td>
                                <td> {{ $match->team2->name }} </td>
                                <td> {{ $match->date_start }} </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    {{ $matches->links('vendor.pagination.bootstrap-4') }}

                    <p>
                        Displaying {{$matches->count()}} of {{ $matches->total() }} match(es).
                    </p>
                </div>
            </div>


        </div>
    </div>
@endsection

@section('script')
    <script>

        $(document).on('change', '#team-1', function () {
            $('#team-2').find('option').removeAttr('disabled');
            $('#team-2 option[value="' + $(this).val() + '"]').attr('disabled', 'disabled');
        }).on('change', '#team-2', function () {
            $('#team-1').find('option').removeAttr('disabled');
            $('#team-1 option[value="' + $(this).val() + '"]').attr('disabled', 'disabled');
        });

    </script>
@endsection
