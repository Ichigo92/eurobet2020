@extends('layouts/master')

@section('title', $title)

@section('content')

    <h3 class="text-center">Match List - Time: <span class="datetime"></span></h3>
    <div class="row">
        @foreach($matches as $match)

            <div class="col-md-6 mb-3">
                <div class="card @if($match->is_playing) border-success @elseif($match->is_finished) border-danger @else border-secondary @endif">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 font-weight-bolder">
                                {{ $match->team1->name }} - {{ $match->team2->name }}
                            </div>
                            <div class="col-md-4">
                               @if($match->is_finished)
                                    @if($match->winner_id === 0)
                                        Pair
                                    @elseif ($match->winner_id === null)
                                        @if(auth()->user() && auth()->user()->is_admin)
                                            Winner: <a href="{{ route('scores', ['theMatch' => $match->id]) }} "> <span class="text-secondary">Not set</span> </a>
                                        @else
                                            Winner:  <span class="text-secondary">Not set</span>
                                        @endif
                                    @else
                                        Winner: <span class="text-success"> {{$match->winnerTeam->name}}</span>
                                    @endif
                                @endif

                            </div>
                            <div class="col-md-4">

                                @if($match->is_playing)
                                    <span class="running-game text-success"><i class="fa fa-play-circle"></i> Playing </span> <span class="text-dark play-time pull-right" data-start="{{ $match->date_start }}"></span>
                                @endif

                                @if($match->is_finished)
                                        <span class="text-danger"><i class="fa fa-close"></i> Finished</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-8">
                                Match start: {{ date('j F, Y H:i', strtotime($match->date_start)) }}
                            </div>

                            @if(auth()->user())

                                @if(date('Y-m-d H:i:s', strtotime('+30 minutes')) > date($match->date_start))
                                    <div class="col-md-4">
                                        <a class="btn btn-primary btn-block btn-sm" href="{{ route('bets-list', $match->id) }}">
                                            Show bets
                                        </a>
                                    </div>
                                @endif
                                @if($match->is_bettable)
                                    <div class="col-md-4">
                                        @if(array_key_exists($match->id, $user_bets))
                                            <span class="text-danger">Already bet</span>
                                        @else
                                            <a class="btn btn-block btn-outline-primary btn-sm"
                                               href="{{ route('match-bet', $match->id) }}">Bet</a>
                                        @endif
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-12 d-flex justify-content-center">
            {{ $matches->links('vendor.pagination.bootstrap-4') }}
        </div>
        <div class="col-md-12">
            <p>
                Displaying {{$matches->count()}} of {{ $matches->total() }} Match(s).
            </p>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function() {
            setInterval(function() {
                var momentNow = moment();
                $('.datetime').html(momentNow.format('DD/MM/YYYY - HH:mm:ss'));

                if (momentNow.format('ss') === '00') {
                    setTimeout(function () {
                    window.location.reload();
                    }, 1000)
                }
            }, 100);

            setInterval(function () {
                $('.play-time').each(function (){
                    let start = moment($(this).data('start'));
                    $(this).html(pad(moment.duration(moment().diff(start)).hours()) +':'+pad(moment.duration(moment().diff(start)).minutes()) +':'+ pad(moment.duration(moment().diff(start)).seconds()));
                })
            }, 500)
        });

        function pad (str, max) {
            if (!max) max = 2
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>
@endsection
