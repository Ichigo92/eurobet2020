@extends('layouts/master')

@section('title', $title)

@section('content')
    <main class="signup">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="card">
                        <h3 class="card-header text-center">Login</h3>
                        <div class="card-body">

                            <form action="{{ route('login') }}" method="POST">
                                @csrf

                                <div class="form-group mb-3">
                                    <input type="text" placeholder="Email" id="email_address" class="form-control"
                                           name="email"  autofocus>
                                    @if ($errors->has('email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>

                                <div class="form-group mb-3">
                                    <input type="password" placeholder="Password" id="password" class="form-control"
                                           name="password" >
                                    @if ($errors->has('password'))
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>

                                <div class="d-grid mx-auto">
                                    <button type="submit" class="btn btn-dark btn-block">Login</button>
                                </div>
                                @if ($errors->has('login-error'))
                                    <span class="text-danger">{{ $errors->first('login-error') }}</span>
                                @endif
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
