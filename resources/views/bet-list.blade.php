@extends('layouts/master')

@section('title', $title)

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h3 class="card-title text-center">Bet List for match #{{ $match->id }} - {{ $match->team1->name }} <i class="fa fa-close fa-fw"></i> {{ $match->team2->name }} </h3>
                <div class="card-body">

                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>User</th>
                            <th>Bet</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($match->bets->count() == 0)
                            <tr>
                                <td colspan="4">No Bet to display.</td>
                            </tr>
                        @endif


                        @foreach($match->bets as $index => $bet)

                            <tr>
                                <td>{{ ($match->bets->currentpage()-1) * $match->bets->perpage() + $loop->index + 1 }}</td>
                                <td>{{ $bet->user->name }}</td>
                                <td>{{ $bet->betTeam->name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    {{ $match->bets->links('vendor.pagination.bootstrap-4') }}

                    <p>
                        Displaying {{$match->bets->count()}} of {{ $match->bets->total() }} team(s).
                    </p>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('script')

@endsection
