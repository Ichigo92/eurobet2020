<footer class="page-footer font-small pt-4 mt-5 bg-dark">
    <div class="container">
        <ul class="list-unstyled list-inline text-center">
            <li class="list-inline-item">
                <a class="btn-floating btn-ins mx-1" href="{{ route('homepage') }}">
                    <i class="fa fa-home"> </i> EuroBet2020.com
                </a>
            </li>
            <li class="list-inline-item">
                <a class="btn-floating btn-ins mx-1" href="https://instagram.com">
                    <i class="fa fa-instagram"> </i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="btn-floating btn-fb mx-1" href="https://facebook.com">
                    <i class="fa fa-facebook-f"> </i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="btn-floating btn-tw mx-1" href="https://twitter.com">
                    <i class="fa fa-twitter"> </i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="btn-floating btn-gplus mx-1" href="https://pintrest.com">
                    <i class="fa fa-pinterest"> </i>
                </a>
            </li>
        </ul>
    </div>
</footer>
