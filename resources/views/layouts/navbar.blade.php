<nav class="navbar mb-3 navbar-toggleable-md navbar-expand-lg bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>


    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('homepage') }}">
                    <i class="fa fa-home fa-fw"></i> Homepage
                </a>
            </li>

            @if(auth()->check() )
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('rankings') }}">
                        <i class="fa fa-list-ol fa-fw"></i> Rankings
                    </a>
                </li>
                @if(auth()->user()->is_admin)
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('teams') }}">
                            <i class="fa fa-address-card fa-fw"></i> Teams
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('matches') }}">
                            <i class="fa fa-futbol-o fa-fw"></i> Matches
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('scores') }}">
                            <i class="fa fa-star fa-fw"></i> Scores
                        </a>
                    </li>
                @endif
            @endif
        </ul>
    </div>

    @if(auth()->check())
        <div class="pull-right">
            <a href="#">
                {{ auth()->user()->name }} <i class="fa fa-user fa-fw"></i>
            </a>
        </div>
        <d class="pull-right ml-5">
            <a href="{{ route('logout') }}">
                Logout
                <i class="fa fa-sign-out fa-fw"></i>
            </a>
        </d>
    @else
        <div class="pull-right">
            <a href="{{ route('login') }}">
                <i class="fa fa-sign-in-alt"></i>
                <span class="clearfix d-none d-sm-inline-block">Login</span>
            </a>
        </div>
        <div class="pull-right ml-5">
            <a href="{{ route('signin') }}">
                <i class="fa fa-user-plus"></i>
                <span class="clearfix d-none d-sm-inline-block">Register</span>
            </a>
        </div>
    @endif

</nav>
