<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EuroBet2020 - @yield('title', '')</title>
    <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">

    <style>

    </style>
</head>

<body class="fixed-sn">

@include('layouts.navbar')

<main role="main" class="flex-wrapper">
    <div class="container">

        @yield('content')

    </div>

    @include('layouts.footer')
</main>

</body>



@include('layouts.scripts')
@yield('script')

</html>
