@extends('layouts/master')

@section('title', $title)

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h3 class="card-header text-center">User Ranking</h3>
                <div class="card-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <th>User Name</th>
                        <th>Points</th>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->points }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    {{ $users->links('vendor.pagination.bootstrap-4') }}

                    <p>
                        Displaying {{$users->count()}} of {{ $users->total() }} user(s).
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection
