#How run the project

First of all you have to install the project depencies, so enter into project and run
<code>composer install </code>

The project is developed on Laravel Homestead, so you have to follow this instruction: [Laravel Homestead](https://laravel.com/docs/8.x/homestead#installation-and-setup)

Follow the instruction until <b>Configuring Homestead</b> section

Then copy the Homestead.yaml file into Homestead project and replace the original Homestead.yaml.

now, into Homestead.yaml file edit the "folder" properties and configure shared folders as necessary:

    folders:
        - map: ~/code/project1
        to: /home/vagrant/project1

Move into Homestead project with
<code>cd ~/Homestead </code>

and now here you can turn up the machine with command <code>vagrant up</code>

If everything goes well you can reach vagrant machine using the following command:

<code>vagrant ssh</code>

Inside the machine the default version of PHP is 8.0.5 so we change it into 7.3, run:

<code>php73</code>

Now we can enter into project directory and type the following command for create all tables we need:

<code>php artisan migrate</code>

If you want some example values for your tables you can run the following command for the db seeding:

<code>php artisan db:seed</code>

By default you can access at your application with browser entering the following URL http://192.168.10.10

Now you can have fun with the application!
