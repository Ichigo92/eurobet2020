<?php

namespace Database\Factories;

use App\Models\Team;
use App\Models\TheMatch;
use Faker\Generator;
use Illuminate\Database\Eloquent\Factories\Factory;

class TheMatchFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TheMatch::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $team1 = Team::all()->random()->id;
        $team2 = Team::all()->except($team1)->random()->id;

        return [
            'team_1' => $team1,
            'team_2' => $team2,
            'date_start' => $this->faker->dateTimeBetween('-3 hour', '+1 hour')->format('Y-m-d H:i:00')
        ];


    }
}
