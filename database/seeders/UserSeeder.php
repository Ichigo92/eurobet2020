<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name' => 'EuroBet2020',
            'email' => 'eurobet2020@gmail.com',
            'password' => Hash::make(env('ADMIN_PASSWORD')),
            'is_admin' => true,
            'email_verified_at' => Carbon::now(),
            'remember_token' => Str::random(10),
        ]);

        User::factory()->count(10)->create();
    }
}
