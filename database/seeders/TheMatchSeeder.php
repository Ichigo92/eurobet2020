<?php

namespace Database\Seeders;

use App\Models\TheMatch;
use Illuminate\Database\Seeder;

class TheMatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TheMatch::factory()->count(20)->create();
    }
}
